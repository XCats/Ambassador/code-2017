from evdev import InputDevice, categorize, ecodes
import serial

ser = serial.Serial('/dev/ttyACM0',9600,timeout=1) #open /dev/ttyACM0
print(ser.name) #print name of port to confirm we have right port

gamepad = InputDevice('/dev/input/event0') #REVERT TO event0 if this doesn't work

print(gamepad)

out1Pos = 100
out2Pos = 100
for event in gamepad.read_loop():
	if event.type == ecodes.EV_ABS:
		if event.code == 1:
			out1Pos = int(((-1*event.value) + 32767) / 327.67)
			out = '0:' + str.format("{:03d}",out1Pos) + '&'
			print(out)
			ser.write(out.encode())
		if event.code == 4:
			out2Pos = int(((-1*event.value) + 32767) / 327.67)
			out = '1:' + str.format("{:03d}",out2Pos) + '&'
			print(out)
			ser.write(out.encode())
		ser.flushInput()
		ser.flushOutput()
'''	if event.type == 1:
		if event.code == 310:
			print('2:001&')
		if event.code == 311:
			print('3:001&')'''
