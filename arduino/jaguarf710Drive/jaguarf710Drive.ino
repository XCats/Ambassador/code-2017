#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include <Servo.h>
#define INPUT_SIZE 11

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();
Servo armServo;
Servo clawServo;
//Constant 16 bit integers hold pulse widths out of 4095
const uint16_t neutral = 704; //1.5 millisecond Pulse  | For frequency 123  - 667 steps for frequency 100 - 614 steps
const uint16_t fullReverse = (uint16_t)(.67*neutral/1.5);
const uint16_t fullForward = (uint16_t)(2.33 * neutral/1.5);

const int startPos = 0;
const int maxPos = 0;
int armPos = 0;
int clawPos = 0;
//

//Constant 8 bit integers hold motor pins on pwm breakout
//const uint8_t lMotorPin = 1;
//const uint8_t rMotorPin = 0;

//This runs once to set up everything that we need
void setup() {
  pwm.begin(); //Start the interface between the Arduino and the pwm breakout board
  pwm.setPWMFreq(123); //Set the frequency of the PWM to 123 hz
  allStop();
  Serial.begin(9600);
  armServo.attach(10);
}

//This code runs over and over, repeating whatever is inside of it
void loop() {
  char input[INPUT_SIZE + 1];
  byte size = Serial.readBytes(input, INPUT_SIZE);

  input[size] = 0;

  char* command = strtok(input, "&");
  while(command!=0){
    char* seperator = strchr(command, ':');
    if(seperator != 0){
      *seperator = 0;
      int motorID = atoi(command);
      ++seperator;
      int velocity = atoi(seperator);
      if(motorID == 0 || motorID == 1){
        if(velocity > 100){
         motorForward(velocity - 100, motorID); 
        }
        else if(velocity == 100){
         motorNeutral(motorID);
        }
        else if(velocity < 100){
         motorReverse(100 - velocity, motorID);
        }
      }
      else if(motorID == 2){
        armPos--;
        armServo.write(armPos);
      }
      else if(motorID == 3){
        armPos++;
        armServo.write(armPos);
      }
    }
    Serial.println("OK");
    command = strtok(0,"&");
  }
}
//This code handles moving the motor forward. It takes an input velocity (a percentage) along with a motor pin defining which motor we want to use
int motorForward(int velocity, int pin){
  int range = fullForward-neutral; //This determines how far it is between full forward and neutral
  double velocitySpecial = (double)velocity/100.0; //This sets the velocity to a deciaml between 0 and one instead of between 1 and 100
  int absSpeed = round((double)range*velocitySpecial); //This detremines how far above neutral our speed actually is
  int outSpeed = neutral + absSpeed; //This sets the pulse that we want to send to the PWM Board
  pwm.setPWM(pin,0,outSpeed); //Sends the pulse to the PWM board
  return outSpeed; //Lets us access the speed
}

//This code handles reversing the motor backward.
int motorReverse(int velocity, int pin){
  int range = neutral - fullReverse;
  double velocitySpecial = (double)velocity/100.0;
  int absSpeed = round((double)range*velocitySpecial);
  int outSpeed = neutral - absSpeed;
  pwm.setPWM(pin,0,outSpeed);
  return outSpeed; //Lets us access what the speed is
 }

//This code sets the motor to neutral
void motorNeutral(int pin){
 pwm.setPWM(pin,0,neutral); 
}
void allStop(){
  for(uint8_t i = 0; i < 16; i++){
    motorNeutral(i);
  }
}


